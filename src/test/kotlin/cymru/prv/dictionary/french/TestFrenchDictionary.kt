package cymru.prv.dictionary.french

import cymru.prv.dictionary.common.DictionaryList
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path


class TestFrenchDictionary {

    @Test
    fun `dictionary should initialise properly`(){
        assertDoesNotThrow {
            val dict = FrenchDictionary(DictionaryList())
            assertTrue(dict.numberOfWords > 0)
        }
    }

    @Test
    @Throws(IOException::class)
    fun ensureThatVersionNumbersAreTheSame() {
        for (line in Files.readAllLines(Path.of("build.gradle"))) {
            if (line.startsWith("version")) {
                val version = line
                    .split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    .replace("'", "")
                Assertions.assertEquals(version, FrenchDictionary().version)
                return
            }
        }
        Assertions.fail<Any>("Version number not found in build.gradle")
    }

}